<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class nhansu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	public function index()

	{
		$this->load->model('nhansu_model');
		$ketqua =$this->nhansu_model->getAllData();
		$ketqua  = ['mangketqua' => $ketqua];
		$this->load->view('nhansu_view', $ketqua);
		
	}
	public function nhansu_add()
	{

		//load model
		$this->load->model('nhansu_model');
		 //upload file ảnh
		
		$target_dir = "Fileupload/";
		$target_file = $target_dir . basename($_FILES["avatar"]["name"]);
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["avatar"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "file không phải là ảnh";
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    echo "ảnh đã tồn tai";
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["avatar"]["size"] > 5000000) {
		    echo "dung lượng quá lớn";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
		&& $imageFileType != "gif" ) {
		    echo "không đúng định dạng yêu cầu";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "chưa chọn file";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {
		    //    echo "The file ". basename( $_FILES["avatar"]["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}
		
		//lấy dữ lieu tư view
		$tuoi = $this->input->post('tuoi');
		$ten =$this->input->post('ten');
		$sdt = $this->input->post('sdt');
		$sodonhang = $this->input->post('sodonhang');
		$linkfb = $this->input->post('sodonhang');
		$anhavatar = base_url()."/Fileupload" . basename($_FILES["avatar"]["name"]);

		

		$trangthai = $this->nhansu_model->inserDataToMysql($ten, $tuoi, $sdt, $anhavatar, $linkfb, $sodonhang);  

		if ($trangthai) {
		echo "insert thành công";
		}else{
			echo "thất bại";
		}
		
	}
	
}

/* End of file nhansu.php */
/* Location: ./application/controllers/nhansu.php */