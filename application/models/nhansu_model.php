<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class nhansu_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}
	public function inserDataToMysql($ten, $tuoi, $sdt, $anhavatar, $linkfb, $sodonhang)
	{
		//xu ly thong tin nhan về từ controler
		$dulieu = array(
			'ten' => $ten, 
			'tuoi' => $tuoi, 
			'sdt' => $sdt, 
			'avatar' => $anhavatar, 
			'linkfb' => $linkfb, 
			'sodonhang' => $sodonhang 
		);

		$this->db->insert('nhan_vien', $dulieu);
		 return $this->db->insert_id();
	}
	public function getAllData()
	{
		$this->db->select('*');
		 $dulieu = $this->db->get('nhan_vien');
		 $ketqua = $dulieu->result_array();
		 return $ketqua;
	}

}

/* End of file nhansu_model.php */
/* Location: ./application/models/nhansu_model.php */