<!DOCTYPE html>
<html lang="en"><head>
	<title> Example </title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">  
	<script type="text/javascript" src="<?php echo base_url(); ?>vendor/bootstrap.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>vendor/holder.min.js"></script>
 	<script type="text/javascript" src="<?php echo base_url(); ?>1.js"></script>
	<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>vendor/font-awesome.css">
 	<link rel="stylesheet" href="<?php echo base_url(); ?>1.css">
</head>
<body >
 <div class="container">
 	<div class="text-xs-center">
 		<h3 class="display-3">danh sách nhân sự</h3>
 	</div>
 <div class="container">
 	<div class="row">
		<div class="card-columns">

			<?php foreach ($mangketqua as $value): ?>
				
			
			<div class="card">
				<img class="card-img-top img-fluid" data-src="<?php $value['avatar'] ?>" alt="Card image cap">
				<div class="card-block">
					<h4 class="card-title ten">Lê Tuấn Anh</h4>
					<p class="card-text tuoi">tuổi: <b>22</b></p>
					<p class="card-text sđt">tel: <b>01673631368</b></p>
					<p class="card-text sodonhang">số đơn hàng đã giao: <b>15</b></p>
					<p class="card-text linkfb"><a href="" class="btn btn-secondary btn-xs">Facebook <i class="fa fa-chevron-right"></i> </a></p>
				</div>
			</div>
			<?php endforeach ?>

		</div>
 	</div>
 </div>	
 <div class="container">
 	<div class="text-xs-center">
 		<h3 class="display-3">danh sách nhân sự</h3>
 	</div>
</div>
 	<form method="post" enctype="multipart/form-data" action="<?= base_url() ?>/index.php/nhansu/nhansu_add">
 		<div class="row">
 			<div class="col-sm-6">
 					<div class="form-group row">
		 			<label for="anh" class="col-sm-4 form-control-label text-xs-center">ảnh AVT</label>
		 			<div class="col-sm-8">
		 				<input name="avatar" type="file" class="form-control" id="avatar" placeholder="upload ảnh">
		 			</div>
 					</div>
 			</div>
 			<div class="col-sm-6">
 				
			 		<div class="form-group row">
			 			<label for="ten" class="col-sm-4 form-control-label text-xs-center">Tên nhân viên</label>
			 			<div class="col-sm-8">
			 				<input name="ten" type="text" class="form-control" id="ten" placeholder="tên">
			 			</div>
			 		</div>
 			</div>
 			
 		</div>
 	
		<div class="row">
			<div class="col-sm-6">
 				
			 		<div class="form-group row">
			 			<label for="linkfb" class="col-sm-4 form-control-label text-xs-center">link fb</label>
			 			<div class="col-sm-8">
			 				<input name="linkfb" type="text" class="form-control" id="linkfb" placeholder="linkfb">
			 			</div>
			 		</div>
 			</div>
 			<div class="col-sm-6">
 					<div class="form-group row">
			 			<label for="tuoi" class="col-sm-4 form-control-label text-xs-center">Tuổi nhân viên</label>
			 			<div class="col-sm-8">
 						<input name="tuoi" type="text" class="form-control" id="tuoi" placeholder="tuổi">
			 			</div>
			 		</div>
 			</div>
		</div>

 		<div class="row">
 			
 			<div class="col-sm-6">
 					<div class="form-group row">
			 			<label for="sdt" class="col-sm-4 form-control-label text-xs-center">Số điện thoại</label>
			 			<div class="col-sm-8">
 						<input name="sdt" type="text" class="form-control" id="sdt" placeholder="số điện thoại">
			 			</div>
			 		</div>
 			</div>
 			<div class="col-sm-6">
 					<div class="form-group row">
			 			<label for="sodonhang" class="col-sm-4 form-control-label text-xs-center">Số đơn hàng</label>
			 			<div class="col-sm-8">
 						<input name="sodonhang" type="text" class="form-control" id="sodonhang" placeholder="số đơn hàng">
			 			</div>
			 		</div>
 			</div>
 		</div>
 	
 		
 		
 		<div class="form-group row text-xs-center">
 			<div class="col-sm-offset-2 col-sm-10">
 				<button type="submit" class="btn btn-success ">Thêm mới</button>
 				<button type="reset" class="btn btn-danger ">Nhập lại</button>
 			</div>
 		</div>
 	</form>
 
 </div>
</body>
</html>